scalaVersion in ThisBuild := "2.12.6"

lazy val commonSettings = Seq(
  organization := "io.plasma",
  scalaVersion := "2.12.6",
  version := "0.1.0-SNAPSHOT",
)

lazy val root = (project in file("."))
  .aggregate(matrixCoreApi, matrixClientApi, homeServer)
  .settings(
    name := "plasma"
  )

// Plasma utilities
lazy val matrixUtils = (project in file ("matrix-utils"))
  .settings(commonSettings: _*)
  .settings(
    name := "matrix-utils",
    libraryDependencies := Dependencies.utilsDependencies
  )


// Plasma SDK
lazy val matrixCoreApi = (project in file ("matrix-core-api"))
  .settings(commonSettings: _*)
  .settings(
    name := "matrix-core-api",
    libraryDependencies := Dependencies.commonDependencies
  )

// Matrix client API implementation
lazy val matrixClientApi = (project in file ("matrix-client-api"))
  .settings(commonSettings: _*)
  .settings(
    name := "matrix-client-api",
    libraryDependencies := Dependencies.commonDependencies
  )
  .dependsOn(matrixCoreApi, matrixClientApiModel)

// Matrix client API model
lazy val matrixClientApiModel = (project in file ("matrix-client-api-model"))
  .settings(commonSettings: _*)
  .settings(
    name := "matrix-client-api-model",
    libraryDependencies := Dependencies.commonDependencies
  )
  .dependsOn(matrixCoreApi)

// Matrix identity API client implementation
lazy val matrixIdentityApi = (project in file ("matrix-identity-api"))
  .settings(commonSettings: _*)
  .settings(
    name := "matrix-identity-api",
    libraryDependencies := Dependencies.commonDependencies
  )
  .dependsOn(matrixCoreApi, matrixIdentityApiModel)

// Matrix client API model
lazy val matrixIdentityApiModel = (project in file ("matrix-identity-api-model"))
  .settings(commonSettings: _*)
  .settings(
    name := "matrix-identity-api-model",
    libraryDependencies := Dependencies.commonDependencies
  )
  .dependsOn(matrixCoreApi)


// Plasma Home Server
lazy val homeServer = (project in file ("homeserver"))
  .enablePlugins(PlayScala)
  .enablePlugins(BuildInfoPlugin)
  .settings(commonSettings: _*)
  .settings(
    name := "plasma-homeserver",
    libraryDependencies ++= Seq(guice, jdbc),
    libraryDependencies ++= Dependencies.hsDependencies
  )
  .settings(
    buildInfoPackage := "io.plasma.hs.version",
    buildInfoObject := "BuildInfo",
    buildInfoKeys := Seq[BuildInfoKey](name, version, "projectName" -> "plasma-hs"),
    buildInfoOptions += BuildInfoOption.BuildTime,
  )
  .dependsOn(matrixUtils, matrixCoreApi, matrixClientApiModel)

onLoad in Global ~= (_ andThen ("project homeServer" :: _))