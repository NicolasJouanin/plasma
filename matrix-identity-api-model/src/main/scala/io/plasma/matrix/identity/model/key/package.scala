package io.plasma.matrix.identity.model

package object key {
  case class PubKeyResponse(public_key: String)
  case class IsValidResponse(valid: Boolean)
}
