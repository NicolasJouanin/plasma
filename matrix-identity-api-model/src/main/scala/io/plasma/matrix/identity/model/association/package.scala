package io.plasma.matrix.identity.model

package object association {
  case class LookupResponse(address: Option[String],
                            medium: Option[String],
                            mxid: Option[String],
                            not_before: Option[Int],
                            not_after: Option[Int],
                            ts: Option[Int],
                            signatures: Map[String, Map[String, String]])
}
