import sbt._

object Dependencies {
  object V {
    val circe        = "0.9.3"
    val logback      = "1.2.3"
    val scalaLogging = "3.9.0"
    val akkaHttp     = "10.1.1"
    val akka         = "2.5.13"
    val scalaGuice   = "4.1.1"
    val quillJdbc    = "2.4.2"
    val flyway       = "5.0.0"
    val postgresql   = "42.2.2"
    val playMailer   = "6.0.1"
    val scalaTest    = "3.0.5"
    val commonsCodec = "1.11"
    val argon2       = "2.4"
  }

  val commonDependencies: Seq[ModuleID] = Seq(
    "ch.qos.logback"             % "logback-classic"    % V.logback,
    "com.typesafe.scala-logging" %% "scala-logging"     % V.scalaLogging,
    "com.typesafe.akka"          %% "akka-http"         % V.akkaHttp,
    "com.typesafe.akka"          %% "akka-stream"       % V.akka,
    "com.typesafe.akka"          %% "akka-http-testkit" % V.akkaHttp % Test,
    "org.scalatest"              %% "scalatest"         % V.scalaTest % Test
  ) ++ Seq(
    "io.circe" %% "circe-core",
    "io.circe" %% "circe-generic",
    "io.circe" %% "circe-parser"
  ).map(_ % V.circe)

  val sdkDependencies: Seq[ModuleID] = commonDependencies ++ Seq()

  val hsDependencies: Seq[ModuleID] = commonDependencies ++ Seq(
    "com.typesafe.akka" %% "akka-cluster"          % V.akka,
    "com.typesafe.akka" %% "akka-cluster-sharding" % V.akka,
    "net.codingwell"    %% "scala-guice"           % V.scalaGuice,
    "io.getquill"       %% "quill-jdbc"            % V.quillJdbc,
    "org.flywaydb"      %% "flyway-play"           % V.flyway,
    "org.postgresql"    % "postgresql"             % V.postgresql,
    "com.typesafe.play" %% "play-mailer"           % V.playMailer,
    "com.typesafe.play" %% "play-mailer-guice"     % V.playMailer,
    "de.mkammerer"      % "argon2-jvm"             % V.argon2
  )

  val utilsDependencies: Seq[ModuleID] = commonDependencies ++ Seq(
    "com.typesafe.akka" %% "akka-actor"   % V.akka,
    "commons-codec"     % "commons-codec" % V.commonsCodec,
    "com.typesafe.akka" %% "akka-testkit" % V.akka % Test
  )
}
