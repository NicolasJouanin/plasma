package io.plasma.matrix.client.r0.model

import io.circe.Json

package object events {

  case class SyncResponse(next_batch: String,
                          rooms: Rooms,
                          presence: Presence,
                          account_data: AccountData,
                          to_device: ToDevice,
                          device_lists: DeviceLists)

  case class Rooms(join: Map[String, JoinedRoom], invite: Map[String, InvitedRoom], leave: Map[String, LeftRoom])
  case class JoinedRoom(state: State,
                        timeline: Timeline,
                        ephemeral: Ephemeral,
                        account_data: AccountData,
                        unread_notifications: UnreadNotificationsCount)
  case class InvitedRoom(invite_state: InviteState)
  case class LeftRoom(state: State, timeline: Timeline)
  case class InviteState(events: List[Event])
  case class State(events: List[Event])
  case class Timeline(events: List[Event], limited: Boolean, prev_batch: String)
  case class Ephemeral(events: List[Event])
  case class UnreadNotificationsCount(higher_count: Option[Int], notification_count: Option[Int])

  case class Presence(events: List[Event])
  case class AccountData(events: List[Event])
  case class ToDevice(events: List[DeviceEvent])
  case class DeviceEvent(content: Json, sender: String, `type`: String)
  case class DeviceLists(changed: List[String])
  case class Event(event_id: Option[String],
                   content: Json,
                   origin_server_ts: Option[Long],
                   sender: Option[String],
                   state_key: Option[String],
                   `type`: String,
                   unsigned: Option[Unsigned])
  case class Unsigned(age: Int,
                      prev_content: Option[Json],
                      transaction_id: Option[String],
                      redacted_because: Option[Event])

  case class StateEvent(content: Json,
                        `type`: String,
                        event_id: String,
                        room_id: String,
                        sender: String,
                        origin_server_ts: String,
                        unsigned: Unsigned,
                        prev_content: Json,
                        state_key: String)

  case class RoomMembersResponse(chunk: List[MemberEvent])
  case class MemberEvent(content: MemberEventContent,
                         `type`: String,
                         event_id: String,
                         room_id: String,
                         sender: String,
                         origin_server_ts: String,
                         unsigned: UnsignedData,
                         prev_content: Option[MemberEventContent],
                         state_key: String,
                         invite_room_state: List[StrippedState])
  case class UnsignedData(age: Int, related_because: Option[Event], transaction_id: String)
  case class MemberEventContent(avatar_url: String,
                                displayname: Option[String],
                                membership: String,
                                is_direct: Boolean,
                                third_party_invite: Invite)
  case class StrippedState(content: MemberEventContent, state_key: String, `type`: String)
  case class Invite(display_name: String, signed: Signed)
  case class Signed(mxid: String, signatures: Map[String, Map[String, String]], token: String)

  case class JoinedMembersResponse(joined: List[RoomMember])
  case class RoomMember(display_name: String, avatar_url: String)

  case class RoomMessagesResponse(start: String, end: String, chunk: List[RoomEvent])
  case class RoomEvent(content: Json,
                       `type`: String,
                       event_id: String,
                       room_id: String,
                       sender: String,
                       origin_server_ts: String,
                       unsigned: UnsignedData)

  case class SendRoomEventResponse(event_id: String)

  case class RedactEventRequest(reason: String)
  case class RedactEventResponse(event_id: String)
}
