package io.plasma.matrix.client.r0.model

package object auth {
  case class Flow(stages: List[String])
  case class AuthFlow(completed: Option[List[String]] = None,
                      errcode: Option[String] = None,
                      error: Option[String] = None,
                      flows: List[Flow],
                      params: Map[String, Map[String, String]] = Map.empty,
                      session: Option[String] = None)

  case class LoginRequest(`type`: String,
                          user: Option[String] = None,
                          medium: Option[String] = None,
                          address: Option[String] = None,
                          password: Option[String] = None,
                          token: Option[String] = None,
                          device_id: Option[String] = None,
                          initial_device_display_name: Option[String] = None)

  case class LoginResponse(user_id: String, access_token: String, home_server: String, device_id: String)
}
