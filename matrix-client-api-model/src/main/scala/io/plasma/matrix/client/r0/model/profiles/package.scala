package io.plasma.matrix.client.r0.model

package object profiles {
  case class SetDisplayNameRequest(displayname: Option[String])
  case class GetDisplayNameResponse(displayname: Option[String])
  case class SetAvatarUrlRequest(avatar_url: Option[String])
  case class GetAvatarUrlResponse(avatar_url: Option[String])
  case class GetProfileResponse(avatar_url: Option[String], displayname: Option[String])
}
