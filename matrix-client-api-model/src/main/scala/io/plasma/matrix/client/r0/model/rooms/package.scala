package io.plasma.matrix.client.r0.model

package object rooms {
  case class CreateRoomRequest(preset: String,
                               visibility: Option[String] = None,
                               room_alias_name: Option[String] = None,
                               name: Option[String] = None,
                               topic: Option[String] = None,
                               invite: Option[List[String]] = None,
                               invite_3pid: Option[List[Invite3Pid]] = None,
                               initial_state: Option[List[StateEvent]] = None,
                               is_direct: Option[Boolean] = None)

  case class Invite3Pid(id_server: String, medium: String, address: String)
  case class StateEvent(`type`: Option[String] = None, state_key: Option[String] = None, content: Option[String] = None)

  case class CreateRoomResponse(room_id: String)

  case class CreateRoomAliasRequest(room_id: String)

  case class ResolveRoomResponse(room_id: String, servers: List[String])

  case class JoinedRoomsResponse(joined_rooms: List[String])

  case class InviteRoomRequest(user_id: String)

  case class JoinRoomRequest(third_party_signed: ThirdPartySigned)
  case class ThirdPartySigned(sender: String,
                              mixid: String,
                              token: String,
                              signatures: Map[String, Map[String, String]])
  case class JoinRoomResponse(room_id: String)

  case class KickUserRequest(user_id: String, reason: Option[String])
  case class BanUserRequest(user_id: String, reason: Option[String])
  case class UnBanUserRequest(user_id: String)

  case class ListPublicRoomsResponse(chunk: List[PublicRoomsChunk],
                                     next_batch: Option[String],
                                     prev_batch: Option[String],
                                     total_room_count_estimate: Option[Int])

  case class PublicRoomsChunk(aliases: Option[List[String]],
                              canonical_alias: Option[String],
                              name: Option[String],
                              num_joined_members: Int,
                              room_id: String,
                              topic: Option[String],
                              world_readable: Boolean,
                              guest_can_join: Boolean,
                              avatar_url: Option[String])

  case class PublicRoomsWithFilterRequest(limit: Option[Int], since: Option[String], filter: Option[Filter])
  case class Filter(generic_search_term: Option[String])

}
