package io.plasma.matrix.client.r0.model

package object account {
  case class AuthData(`type`: String, session: Option[String] = None, example_credential: Option[String] = None)
  case class RegisterRequest(auth: Option[Map[String, String]],
                             bind_email: Option[Boolean],
                             username: Option[String],
                             password: Option[String],
                             device_id: Option[String],
                             initial_device_display_name: Option[String])

  case class RegisterResponse(user_id: String, access_token: String, home_server: String, device_id: String)

  case class RequestTokenRequest(id_server: Option[String], client_secret: String, email: String, send_attempt: Int)

  case class PasswordRequest(new_password: String, auth: Map[String, String])

  case class DeactivateRequest(auth: Map[String, String])

  case class ThreePidIdentifier(medium: String, address: String)
  case class GetThreePidsResponse(threepids: List[ThreePidIdentifier])

  case class PostThreePidRequest(three_pid_creds: ThreePidCredentials, bind: Boolean)
  case class ThreePidCredentials(client_secret: String, id_server: String, sid: String)

  case class WhoAmIResponse(user_id: String)
}
