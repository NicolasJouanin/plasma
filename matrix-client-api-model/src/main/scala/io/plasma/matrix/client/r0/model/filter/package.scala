package io.plasma.matrix.client.r0.model

package object filter {
  case class PostFilterResponse(filter_id: String)
  case class PostFilterRequest(event_fields: Option[List[String]],
                               event_format: String,
                               presence: Filter,
                               account_data: Filter,
                               room: RoomFilter)
  case class Filter(limit: Int,
                    not_senders: Option[List[String]],
                    not_types: Option[List[String]],
                    senders: Option[List[String]],
                    types: Option[List[String]])
  case class RoomFilter(not_rooms: Option[List[String]],
                        rooms: Option[List[String]],
                        ephemeral: RoomEventFilter,
                        include_leave: Boolean,
                        state: RoomEventFilter,
                        timeline: RoomEventFilter,
                        account_data: RoomEventFilter)
  case class RoomEventFilter(limit: Int,
                             not_senders: Option[List[String]],
                             not_types: Option[List[String]],
                             senders: Option[List[String]],
                             types: Option[List[String]],
                             not_rooms: Option[List[String]],
                             rooms: Option[List[String]],
                             contains_url: Boolean)

  case class GetFilterResponse(event_fields: Option[List[String]],
                               event_format: String,
                               presence: Filter,
                               account_data: Filter,
                               room: RoomFilter)
}
