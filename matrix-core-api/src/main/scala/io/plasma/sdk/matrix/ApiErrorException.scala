package io.plasma.sdk.matrix

class ApiErrorException(message: String, errCode: String, error: Option[String]) extends Exception(message) {
  def this(message: String, errCode: String, error: Option[String], cause: Throwable) = {
    this(message, errCode, error)
    initCause(cause)
  }
}
