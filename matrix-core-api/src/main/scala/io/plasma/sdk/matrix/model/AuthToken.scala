package io.plasma.sdk.matrix.model

case class AuthToken(value: String)
