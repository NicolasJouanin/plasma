package io.plasma.sdk.matrix.model

case class ServerError(errcode: String, error: Option[String] = None)
