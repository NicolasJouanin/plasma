package io.plasma.sdk.matrix

case class ServerErrorException(message: String, errcode: String, error: Option[String]) extends Throwable(message)
