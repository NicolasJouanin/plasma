Plasma is a server implementation of the [Matrix protocol](https://matrix.org/). 

Matrix is an open standard for interoperable, decentralised, real-time communication over IP. It can be used to power Instant Messaging, VoIP/WebRTC signalling, Internet of Things communication - or anywhere you need a standard HTTP API for publishing and subscribing to data whilst tracking the conversation history.

# Features

*This sections describes features planned to be implemented, not features currently available. See below for current development status*

Plasma implements server side of current [Matrix API specifications](https://matrix.org/docs/spec/) :
 - [client-server API](https://matrix.org/docs/spec/client_server/r0.3.0.html) (0.3.0)
 - [server-server API](https://matrix.org/docs/spec/server_server/unstable.html) (unstable)
 - [application service API](https://matrix.org/docs/spec/application_service/unstable.html) (unstable)
 - [identity service API](https://matrix.org/docs/spec/identity_service/unstable.html) (unstable)
 - [push gateway API](https://matrix.org/docs/spec/push_gateway/unstable.html) (unstable)

Plasma provides an administration UI dedicated to instance admintrators and provides features like : parameters settings, audit, user and authorization management, ...

# Architecture

Plasma is written in [Scala](https://www.scala-lang.org/) which provides object-oriented and functional programming on the JVM. 

API endpoints are implemented with [Play framework](https://www.playframework.com/) a versatiel and powerful web framework for Scala and Java.

Core backend relies on [Akka](https://akka.io/) technologies like actor model messaging, clustering and streaming to provide a [reactive](https://www.reactivemanifesto.org/), scalable and resilient platform.

Data persistence is delegated to [PostgreSQL](https://www.postgresql.org/) database.

# Current status - 08/06/2018

**THIS PROJECT IS STILL UNDER HEAVY CONSTRUCTION**

## Client-server API

### Common 

- [X] JSON request/response object mapping to scala case class
- [X] API calls binding methods

### Client side

- [X] HTTP client

### Server side 

- [X] `GET /_matrix/client/r0/login` with `m.login.password` authentication type
    - [ ] `m.login.recaptcha`, `m.login.token`, `m.login.oauth2`, `m.login.email.identity`
- [X] `POST /_matrix/client/r0/login` with `m.login.password` authentication method
- [X] `POST /_matrix/client/r0/register` with `m.login.dummy` registration method
    - [ ] `m.login.recaptcha`, `m.login.token`, `m.login.oauth2`, `m.login.email.identity`
- [ ] `POST /_matrix/client/r0/createRoom`
