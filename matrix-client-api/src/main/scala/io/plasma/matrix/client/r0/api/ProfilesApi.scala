package io.plasma.matrix.client.r0.api

import akka.actor.ActorSystem
import akka.http.scaladsl.model.Uri
import akka.stream.Materializer
import io.plasma.matrix.client.r0.model.profiles._
import io.plasma.sdk.matrix.Transport
import io.plasma.sdk.matrix.model.AuthToken
import io.circe.generic.auto._

import scala.concurrent.{ExecutionContextExecutor, Future}

trait ProfilesApi { self: Transport =>
  implicit def system: ActorSystem
  implicit def materializer: Materializer
  implicit def executionContext: ExecutionContextExecutor

  protected def apiRoot: Uri

  def setDisplayName(userId: String, displayName: String)(implicit token: AuthToken): Future[Unit] =
    doPut[SetDisplayNameRequest, Unit](apiRoot.withPath(apiRoot.path + s"/r0/profile/$userId/displayname"),
                                       SetDisplayNameRequest(Some(displayName)))

  def getDisplayName(userId: String)(implicit token: AuthToken): Future[GetDisplayNameResponse] =
    doGet[GetDisplayNameResponse](apiRoot.withPath(apiRoot.path + s"/r0/profile/$userId/displayname"))

  def setAvatarUrl(userId: String, avatarUrl: String)(implicit token: AuthToken): Future[Unit] =
    doPut[SetAvatarUrlRequest, Unit](apiRoot.withPath(apiRoot.path + s"/r0/profile/$userId/avatar_url"),
                                     SetAvatarUrlRequest(Some(avatarUrl)))

  def getAvatarUrl(userId: String)(implicit token: AuthToken): Future[GetAvatarUrlResponse] =
    doGet[GetAvatarUrlResponse](apiRoot.withPath(apiRoot.path + s"/r0/profile/$userId/avatar_url"))

  def getProfile(userId: String)(implicit token: AuthToken): Future[GetProfileResponse] =
    doGet[GetProfileResponse](apiRoot.withPath(apiRoot.path + s"/r0/profile/$userId"))
}
