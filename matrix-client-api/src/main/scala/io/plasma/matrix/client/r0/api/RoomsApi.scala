package io.plasma.matrix.client.r0.api

import akka.actor.ActorSystem
import akka.http.scaladsl.model.Uri
import akka.stream.Materializer
import io.plasma.matrix.client.r0.model.rooms._
import io.plasma.sdk.matrix.Transport
import io.plasma.sdk.matrix.model.AuthToken
import io.circe.generic.auto._

import scala.concurrent.{ExecutionContextExecutor, Future}

trait RoomsApi { self: Transport =>
  implicit def system: ActorSystem

  implicit def materializer: Materializer

  implicit def executionContext: ExecutionContextExecutor

  protected def apiRoot: Uri

  def createRoom(request: CreateRoomRequest)(implicit token: AuthToken): Future[CreateRoomResponse] =
    doPut[CreateRoomRequest, CreateRoomResponse](apiRoot.withPath(apiRoot.path + s"/r0/createRoom"), request)

  def createRoomAlias(roomId: String, roomAlias: String)(implicit token: AuthToken): Future[Unit] =
    doPut[CreateRoomAliasRequest, Unit](apiRoot.withPath(apiRoot.path + s"/r0/directory/room/$roomAlias"),
                                        CreateRoomAliasRequest(roomId))

  def resolveRoomAlias(roomAlias: String)(implicit token: Option[AuthToken] = None): Future[ResolveRoomResponse] =
    doGet[ResolveRoomResponse](apiRoot.withPath(apiRoot.path + s"/r0/directory/room/$roomAlias"))

  def deleteRoomAlias(roomAlias: String)(implicit token: AuthToken): Future[Unit] =
    doDelete[Unit](apiRoot.withPath(apiRoot.path + s"/r0/directory/room/$roomAlias"))

  def joinedRooms(implicit token: AuthToken): Future[JoinedRoomsResponse] =
    doGet[JoinedRoomsResponse](apiRoot.withPath(apiRoot.path + "/r0/joined_rooms"))

  def invite(roomId: String, userId: String)(implicit token: AuthToken): Future[Unit] =
    doPost[InviteRoomRequest, Unit](apiRoot.withPath(apiRoot.path + s"/r0/rooms/$roomId/invite"),
                                    InviteRoomRequest(userId))

  def joinByRoomId(roomId: String, signature: ThirdPartySigned)(implicit token: AuthToken): Future[JoinRoomResponse] =
    doPost[JoinRoomRequest, JoinRoomResponse](apiRoot.withPath(apiRoot.path + s"/r0/rooms/$roomId/join"),
                                              JoinRoomRequest(signature))
  def joinByRoomIdOrAlias(roomIdOrAlias: String, signature: ThirdPartySigned)(
      implicit token: AuthToken): Future[JoinRoomResponse] =
    doPost[JoinRoomRequest, JoinRoomResponse](apiRoot.withPath(apiRoot.path + s"/r0/join/$roomIdOrAlias"),
                                              JoinRoomRequest(signature))

  def leave(roomId: String)(implicit token: AuthToken): Future[Unit] =
    doPost[Unit, Unit](apiRoot.withPath(apiRoot.path + s"/r0/rooms/$roomId/leave"), Unit)

  def forget(roomId: String)(implicit token: AuthToken): Future[Unit] =
    doPost[Unit, Unit](apiRoot.withPath(apiRoot.path + s"/r0/rooms/$roomId/forget"), Unit)

  def kick(roomId: String, userId: String, reason: Option[String] = None)(implicit token: AuthToken): Future[Unit] =
    doPost[KickUserRequest, Unit](apiRoot.withPath(apiRoot.path + s"/r0/rooms/$roomId/kick"),
                                  KickUserRequest(userId, reason))

  def ban(roomId: String, userId: String, reason: Option[String] = None)(implicit token: AuthToken): Future[Unit] =
    doPost[BanUserRequest, Unit](apiRoot.withPath(apiRoot.path + s"/r0/rooms/$roomId/ban"),
                                 BanUserRequest(userId, reason))

  def unban(roomId: String, userId: String)(implicit token: AuthToken): Future[Unit] =
    doPost[UnBanUserRequest, Unit](apiRoot.withPath(apiRoot.path + s"/r0/rooms/$roomId/unban"),
                                   UnBanUserRequest(userId))

  def listPublicRooms(limit: Option[Int] = None,
                      since: Option[String] = None,
                      server: Option[String] = None): Future[ListPublicRoomsResponse] = {
    val query = List(
      limit.map(v => "limit"   -> v.toString),
      since.map(v => "since"   -> v),
      server.map(v => "server" -> v)
    ).filter(_.isDefined).map(_.get).toMap
    doGet[ListPublicRoomsResponse](apiRoot.withPath(apiRoot.path + s"/r0/publicRooms"), query)
  }

  def listPublicRoomsWithFilter(
      limit: Option[Int] = None,
      since: Option[String] = None,
      search_term: Option[String] = None,
      server: Option[String] = None)(implicit token: AuthToken): Future[ListPublicRoomsResponse] = {
    val query = List(
      server.map(v => "server" -> v)
    ).filter(_.isDefined).map(_.get).toMap
    doPost[PublicRoomsWithFilterRequest, ListPublicRoomsResponse](
      apiRoot.withPath(apiRoot.path + s"/r0/publicRooms"),
      PublicRoomsWithFilterRequest(limit, since, search_term.map(t => Filter(Some(t)))),
      query)
  }
}
