package io.plasma.matrix.client.r0

import io.plasma.matrix.client.r0.model.auth.Flow

case class AuthFlowException(message: String,
                             completed: Option[List[String]] = None,
                             errcode: Option[String] = None,
                             error: Option[String] = None,
                             flows: List[Flow],
                             params: Map[String, Map[String, String]] = Map.empty,
                             session: Option[String] = None)
    extends Throwable(message)
