package io.plasma.matrix.client.r0

case class ClientError(errcode: String, error: Option[String])
