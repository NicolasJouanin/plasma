package io.plasma.matrix.client.r0.api

import akka.actor.ActorSystem
import akka.http.scaladsl.model._
import akka.stream.Materializer
import io.circe.generic.auto._
import io.plasma.sdk.matrix.Transport
import io.plasma.matrix.client.r0.model.Versions
import io.plasma.sdk.matrix.model.AuthToken

import scala.concurrent.{ExecutionContextExecutor, Future}

trait BaseApi { self: Transport =>
  implicit def system: ActorSystem
  implicit def materializer: Materializer

  implicit def executionContext: ExecutionContextExecutor
  protected def apiRoot: Uri

  /**
    * Call to GET /_matrix/client/versions
    * @return Versions object
    */
  def serverVersion(implicit token: Option[AuthToken] = None): Future[Versions] =
    doGet[Versions](apiRoot.withPath(apiRoot.path + "/versions"))
}
