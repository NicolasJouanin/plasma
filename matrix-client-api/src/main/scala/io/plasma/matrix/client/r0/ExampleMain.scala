package io.plasma.matrix.client.r0

import akka.actor.ActorSystem
import akka.http.scaladsl.model.Uri
import akka.stream.{ActorMaterializer, Materializer}
import com.typesafe.scalalogging.LazyLogging
import io.circe.{Decoder, Encoder}
import io.plasma.matrix.client.r0.api.{AccountApi, AuthApi, BaseApi, EventsApi}
import io.plasma.matrix.client.r0.model.auth.LoginRequest
import io.plasma.sdk.matrix.model.AuthToken
import io.plasma.sdk.matrix.transports.HttpTransport

import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.util.{Failure, Success}

object ExampleMain extends App with LazyLogging {
  implicit val _system: ActorSystem                        = ActorSystem() //an ActorSystem
  implicit val _materializer: ActorMaterializer            = ActorMaterializer() //and an ActorMaterializer
  implicit val _executionContext: ExecutionContextExecutor = _system.dispatcher

  //val client = new MatrixClient("http://localhost:18008")
  val client = new BaseApi with AuthApi with AccountApi with EventsApi with HttpTransport
  with ClientResponseDecodeHandler {
    override implicit def system: ActorSystem = _system

    override implicit def materializer: Materializer = _materializer

    override implicit def executionContext: ExecutionContextExecutor = _executionContext

    override protected def apiRoot: Uri = Uri("http://localhost:18008").withPath(Uri.Path("/_matrix/client"))

  }
  /*
  client.serverVersion().onComplete {
    case Success(versions) =>
      logger.info(versions.toString)
    //system.terminate()
    case Failure(e) =>
      sys.error("something wrong: " + e)
      _system.terminate()
  }

  client
    .login(LoginRequest("m.login.password", user = Some("nico"), password = Some("nicolas")))
    .flatMap { res =>
      client.whoAmI(res.access_token).flatMap(whoamI => client.logout(res.access_token))
    }
  client
    .register("user", RegisterRequest(Map.empty, false, "user54321", "password", "device1", "My Device"))
    .onComplete {
      case Failure(t: AuthFlowException) =>
        client
          .register(
            "user",
            RegisterRequest(
              Map("type"     -> "m.login.dummy",
                  "session"  -> t.session.get,
                  "response" -> "CAPTCHA",
                  "user"     -> "user12345TT",
                  "password" -> "1password"),
              false,
              "user12345TT",
              "nicolas",
              "device1",
              "My Device"
            )
          )
          .onComplete {
            case r => println(s"!! $r !!")
          }
      case Failure(e) => sys.error("something wrong" + e)
    }
   */
  client
    .login(LoginRequest("m.login.password", user = Some("toto"), password = Some("nicolas")))
    .flatMap { res =>
      implicit val token = AuthToken(res.access_token)
      client
        .sync(timeout = Some(30000))
        .map(response => logger.info(s"$response"))
    }
}
