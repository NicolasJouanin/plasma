package io.plasma.matrix.client.r0.api

import akka.actor.ActorSystem
import akka.http.scaladsl.model.Uri
import akka.stream.Materializer
import io.circe.generic.auto._
import io.plasma.matrix.client.r0.model.filter._
import io.plasma.sdk.matrix.Transport
import io.plasma.sdk.matrix.model.AuthToken

import scala.concurrent.{ExecutionContextExecutor, Future}

trait FilterApi { self: Transport =>
  implicit def system: ActorSystem
  implicit def materializer: Materializer
  implicit def executionContext: ExecutionContextExecutor

  protected def apiRoot: Uri

  def uploadFilter(userId: String, request: PostFilterRequest)(implicit token: AuthToken): Future[PostFilterResponse] =
    doPost[PostFilterRequest, PostFilterResponse](apiRoot.withPath(apiRoot.path + s"/r0/user/$userId/filter"), request)

  def getFilter(userId: String, filterId: String)(implicit token: AuthToken): Future[GetFilterResponse] =
    doGet[GetFilterResponse](apiRoot.withPath(apiRoot.path + s"/r0/user/$userId/filter/$filterId"))
}
