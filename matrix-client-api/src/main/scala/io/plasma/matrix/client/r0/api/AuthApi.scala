package io.plasma.matrix.client.r0.api

import akka.actor.ActorSystem
import akka.http.scaladsl.model.Uri
import akka.stream.Materializer
import io.circe.generic.auto._
import io.plasma.matrix.client.r0.model.auth._
import io.plasma.sdk.matrix.Transport
import io.plasma.sdk.matrix.model.AuthToken

import scala.concurrent.{ExecutionContextExecutor, Future}

trait AuthApi { self: Transport =>
  implicit def system: ActorSystem
  implicit def materializer: Materializer
  implicit def executionContext: ExecutionContextExecutor
  protected def apiRoot: Uri

  /**
    * Call to GET /_matrix/client/versions
    * @return Versions object
    */
  def login(request: LoginRequest): Future[LoginResponse] =
    doPost[LoginRequest, LoginResponse](apiRoot.withPath(apiRoot.path + "/r0/login"), request)

  def logout(implicit token: AuthToken): Future[Unit] =
    doPost[Unit, Unit](apiRoot.withPath(apiRoot.path + "/r0/logout"), None)
}
