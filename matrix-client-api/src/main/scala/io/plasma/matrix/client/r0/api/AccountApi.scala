package io.plasma.matrix.client.r0.api

import akka.actor.ActorSystem
import akka.http.scaladsl.model.Uri
import akka.stream.Materializer
import io.circe.generic.auto._
import io.plasma.matrix.client.r0.model.account._
import io.plasma.sdk.matrix.Transport
import io.plasma.sdk.matrix.model.AuthToken

import scala.concurrent.{ExecutionContextExecutor, Future}

trait AccountApi { self: Transport =>
  implicit def system: ActorSystem
  implicit def materializer: Materializer
  implicit def executionContext: ExecutionContextExecutor

  protected def apiRoot: Uri

  def register(kind: String, request: RegisterRequest): Future[RegisterResponse] =
    doPost[RegisterRequest, RegisterResponse](apiRoot.withPath(apiRoot.path + "/r0/register"),
                                              request,
                                              Map("kind" -> kind))

  def requestToken(request: RequestTokenRequest): Future[Unit] =
    doPost[RequestTokenRequest, Unit](apiRoot.withPath(apiRoot.path + "/r0/register/email/requestToken"), request)

  def requestTokenThreePid(request: RequestTokenRequest): Future[Unit] =
    doPost[RequestTokenRequest, Unit](apiRoot.withPath(apiRoot.path + "/r0/account/3pid/email/requestToken"), request)

  def requestToken(implicit token: AuthToken): Future[Unit] =
    doPost[Unit, Unit](apiRoot.withPath(apiRoot.path + "/r0/account/password/email/requestToken"), Unit)

  def changePassword(request: PasswordRequest): Future[Unit] =
    doPost[PasswordRequest, Unit](apiRoot.withPath(apiRoot.path + "/r0/account/password"), request)

  def deactivate(request: DeactivateRequest): Future[Unit] =
    doPost[DeactivateRequest, Unit](apiRoot.withPath(apiRoot.path + "/r0/account/deactivate"), request)

  def getThreePids(implicit token: AuthToken): Future[GetThreePidsResponse] =
    doGet[GetThreePidsResponse](apiRoot.withPath(apiRoot.path + "/account/3pids"))

  def postThreePid(request: PostThreePidRequest)(implicit token: AuthToken): Future[Unit] =
    doPost[PostThreePidRequest, Unit](apiRoot.withPath(apiRoot.path + "/r0/account/3pid"), request)

  def whoAmI(implicit token: AuthToken): Future[WhoAmIResponse] =
    doGet[WhoAmIResponse](apiRoot.withPath(apiRoot.path + "/r0/account/whoami"))
}
