package io.plasma.matrix.client.r0

import akka.actor.ActorSystem
import akka.http.scaladsl.model.Uri
import akka.stream.Materializer
import io.plasma.matrix.client.r0.api.{AccountApi, AuthApi, BaseApi, FilterApi}
import io.plasma.sdk.matrix.transports.HttpTransport

import scala.concurrent.ExecutionContextExecutor

class MatrixClient(baseUrl: String)(implicit val system: ActorSystem, val materializer: Materializer)
    extends HttpTransport
    with BaseApi
    with AuthApi
    with AccountApi
    with FilterApi with ClientResponseDecodeHandler {

  implicit val executionContext: ExecutionContextExecutor = system.dispatcher
  protected def apiRoot                                   = Uri(baseUrl).withPath(Uri.Path("/_matrix/client"))

}
