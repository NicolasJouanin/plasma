package settings

import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorLogging, ActorRef}
import com.typesafe.scalalogging.LazyLogging
import io.plasma.utils.actors.{ActorCaching, SetCacheDuration}
import javax.inject.{Inject, Named}
import settings.SettingsActor.{CreateSetting, FilterSettings, GetSetting, UpdateSetting}

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._

object SettingsActor {
  case class GetSetting(path: String)
  case class FilterSettings(pathFilter: String)
  case class CreateSetting(setting: Setting)
  case class UpdateSetting(path: String, newValue: Option[String])
}

class SettingsActor @Inject()(settingsRepo: SettingsRepo)
    extends Actor
    with ActorCaching[String, Option[Setting]]
    with ActorLogging {
  import context.dispatcher
  import SettingsActor._
  import akka.pattern.pipe

  override def receive: Receive = super.receive orElse {
    case GetSetting(path) => cacheAndRespond(path, settingsRepo.findByPath(path))
    case CreateSetting(s) => cacheAndRespond(s.path, settingsRepo.create(s).map(s => Some(s)))
    case UpdateSetting(path, newValue) =>
      cacheAndRespond(path, settingsRepo.updateSetting(path, newValue).flatMap(_ => settingsRepo.findByPath(path)))
    case FilterSettings(pathFilter) =>
      settingsRepo.findByPathFilter(pathFilter).pipeTo(sender())
  }
}

class SettingsService @Inject()(@Named("settingsActor") settingsActor: ActorRef)(implicit ec: ExecutionContext)
    extends LazyLogging {
  import akka.pattern.ask
  import io.plasma.utils.actors.defaults._

  def getSetting(path: String): Future[Option[Setting]] = (settingsActor ? GetSetting(path)).mapTo[Option[Setting]]
  def filterSettings(pathFilter: String): Future[List[Setting]] =
    (settingsActor ? FilterSettings(pathFilter)).mapTo[List[Setting]]

  private def valueToString[V](value: V): String = value match {
    case v: FiniteDuration => v.length.toString
    case v: Object         => v.toString
  }
  private def getValue[V](path: String, f: String => V): Future[Option[V]] =
    getSetting(path).map(result => result.map(s => f(s.getStringValue())))

  def getAsString(path: String): Future[Option[String]]   = getValue(path, x => x)
  def getAsInt(path: String): Future[Option[Int]]         = getValue(path, x => x.toInt)
  def getAsLong(path: String): Future[Option[Long]]       = getValue(path, x => x.toLong)
  def getAsBoolean(path: String): Future[Option[Boolean]] = getValue(path, x => x.toBoolean)
  def getAsDouble(path: String): Future[Option[Double]]   = getValue(path, x => x.toDouble)
  def getAsMilliseconds(path: String): Future[Option[FiniteDuration]] =
    getValue(path, x => FiniteDuration(x.toLong, TimeUnit.MILLISECONDS))
  def getAsSeconds(path: String): Future[Option[FiniteDuration]] =
    getValue(path, x => FiniteDuration(x.toLong, TimeUnit.SECONDS))
  def getAsMinutes(path: String): Future[Option[FiniteDuration]] =
    getValue(path, x => FiniteDuration(x.toLong, TimeUnit.MINUTES))

  def createSetting[V](path: String, defaultValue: V, value: Option[V] = None): Future[Unit] = {
    logger.debug(s"Creating Setting($path, $value, $defaultValue")
    (settingsActor ? CreateSetting(Setting(path, value.map(valueToString), valueToString(defaultValue)))).map(_ => Unit)
  }

  def updateSetting[V](path: String, value: Option[V] = None) = {
    (settingsActor ? UpdateSetting(path, value.map(valueToString))).map(_ => Unit)
  }

  private def onStart(): Unit = {
    getAsMinutes("plasma.homeserver.settings.cache.duration").map {
      case Some(duration) => settingsActor ! SetCacheDuration(duration)
      case None           => createSetting("plasma.homeserver.settings.cache.duration", 10.minutes)
    }
  }

  onStart()
}
