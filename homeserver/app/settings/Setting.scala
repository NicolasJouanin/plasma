package settings

case class Setting(path: String, stringValue: Option[String], defaultStringValue: String) {
  def getStringValue() = stringValue.getOrElse(defaultStringValue)

}
