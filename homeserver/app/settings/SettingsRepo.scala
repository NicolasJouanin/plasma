package settings

import com.typesafe.scalalogging.LazyLogging
import db.DbContext
import javax.inject.Inject
import scala.concurrent.duration._

import scala.concurrent.{Await, ExecutionContext, Future}

class SettingsRepo @Inject()(val db: DbContext)(implicit ec: ExecutionContext) extends LazyLogging {
  import db._

  val settings = quote(querySchema[Setting]("settings"))

  def create(setting: Setting) =
    Future {
      val r = run(
        settings
          .insert(lift(setting))
          .onConflictUpdate(_.path)((t, e) => t.stringValue        -> e.stringValue,
                                    (t, e) => t.defaultStringValue -> e.defaultStringValue))
      logger.debug(s"$r")
      r
    }.map(_ => setting)

  def findByPath(path: String) = Future { run(settings.filter(_.path == lift(path))) }.map(r => r.headOption)

  def findByPathFilter(pathFilter: String) =
    Future { run(settings.filter(_.path like lift(pathFilter))) }.map(r => r.toList)

  def updateSetting(path: String, newValue: Option[String]) = Future {
    run(settings.filter(_.path == lift(path)).update(_.stringValue -> lift(newValue)))
  }
}
