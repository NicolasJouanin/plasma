package controllers.client.r0

import auth._
import com.typesafe.scalalogging.LazyLogging
import controllers.AuthenticatedAction
import io.circe.generic.auto._
import io.circe.syntax._
import io.plasma.matrix.client.r0.model.account.{RegisterRequest, RegisterResponse, RequestTokenRequest}
import io.plasma.matrix.client.r0.model.auth.{AuthFlow, Flow, LoginRequest, LoginResponse}
import io.plasma.sdk.matrix.model.ServerError
import io.plasma.utils.{Codecs, MatrixUtils}
import javax.inject.Inject
import play.api.libs.circe.Circe
import play.api.mvc.{BaseController, ControllerComponents, Result}
import server.ServerService

import scala.concurrent.{ExecutionContext, Future}

case class LoginGetResponse(flows: List[LoginGetResponseFlow])
case class LoginGetResponseFlow(`type`: String)

class AuthController @Inject()(val controllerComponents: ControllerComponents,
                               authenticatedAction: AuthenticatedAction,
                               authService: AuthService,
                               serverService: ServerService)(implicit ec: ExecutionContext)
    extends BaseController
    with Circe
    with LazyLogging {

  def loginGet = Action.async {
    authService.genAuthFlow("/login").map { flow =>
      Ok(LoginGetResponse(flow.flows.flatMap(flow => flow.stages.map(LoginGetResponseFlow))).asJson)
    }
  }

  def loginPost = Action.async(circe.tolerantJson[LoginRequest]) { implicit request =>
    request.body match {
      case r @ LoginRequest("m.login.password", _, _, _, _, _, _, _) => doLoginPasswordLogin(r)
      case LoginRequest(auth_type, _, _, _, _, _, _, _) =>
        Future.successful(BadRequest(ServerError("M_UNKNOWN", Some(s"Unsupported login type ${auth_type}")).asJson))
    }
  }

  def logout = authenticatedAction.async { request =>
    authService.logout(request.accessToken).map(_ => Ok("{}"))
  }

  def register(kind: Option[String]) = Action.async(circe.tolerantJson[RegisterRequest]) { implicit request =>
    request.body match {
      case RegisterRequest(None, _, _, _, _, _)     => doSendAuthDataFlows()
      case r: RegisterRequest if r.auth.get.isEmpty => doSendAuthDataFlows()
      case r: RegisterRequest =>
        authService
          .checkAuth(r.auth.get)
          .flatMap {
            case CheckAuthFlowResult(Some(flow), _)  => Future.successful(Unauthorized(flow.asJson))
            case CheckAuthFlowResult(_, Some(error)) => Future.successful(BadRequest(error.asJson))
            case CheckAuthFlowResult(None, None) =>
              kind match {
                case Some("user") | Some("guest") | None =>
                  doRegister(r, kind.getOrElse("user")).map {
                    case Right((account, device)) =>
                      Ok(
                        RegisterResponse(account.mxId,
                                         device.accessToken.get,
                                         serverService.domainName,
                                         device.deviceId).asJson)
                    case Left(error) => BadRequest(error.asJson)
                  }
                case Some(k) =>
                  Future.successful(BadRequest(ServerError("M_UNRECOGNIZED", Some(s"Unrecognized kind '$k'")).asJson))
              }
          }
          .recover {
            case t =>
              logger.error("Authentication check failed", t)
              BadRequest(ServerError("IO.PLASMA.HS.INTERNAL_ERROR", Some(t.getMessage)).asJson)
          }
    }
  }

  def requestToken() = Action.async(circe.tolerantJson[RequestTokenRequest]) { implicit request =>
    Future.successful(BadRequest(ServerError("IO.PLASMA.HS.NOT_YET_IMPLEMENTED").asJson))
  }

  private def doRegister(r: RegisterRequest, kind: String): Future[Either[ServerError, (Account, Device)]] = {
    val isGuest = kind match {
      case "guest" => true
      case _       => false
    }

    authService.createAccount(r.username, r.password, isGuest, false).flatMap { res =>
      res.fold(
        error => Future.successful(Left(error)),
        account =>
          authService
            .createDevice(r.device_id, r.initial_device_display_name, account)
            .map(device => Right((account, device)))
      )
    }
  }

  private def doSendAuthDataFlows(): Future[Result] = {
    authService
      .genAuthFlow("/register")
      .map { authflow =>
        Unauthorized(authflow.asJson)
      }
      .recover {
        case t =>
          logger.error("Auth flow generation failed for /register", t)
          BadRequest(ServerError("IO.PLASMA.HS.INTERNAL_ERROR", Some(t.toString)).asJson)
      }
  }

  private def doLoginPasswordLogin(loginRequest: LoginRequest): Future[Result] = {
    if (loginRequest.user.isEmpty) {
      Future.successful(
        BadRequest(
          ServerError("IO.PLASMA.HS.NOT_IMPLEMENTED",
                      Some("user is mandatory as third party I login is not currently implemented")).asJson))
    } else {
      val user = MatrixUtils.userIdLocalPart(loginRequest.user.get) match {
        case Some(u) => u
        case None    => loginRequest.user.getOrElse("")
      }
      authService.checkLoginPassword(user, loginRequest.password.getOrElse("")).flatMap {
        case Left(error) => Future.successful(BadRequest(error.asJson))
        case Right(account) =>
          authService.getOrCreateDevice(account, loginRequest.device_id, loginRequest.initial_device_display_name).map {
            device =>
              device.accessToken match {
                case Some(token) =>
                  Ok(LoginResponse(account.mxId, token, serverService.domainName, device.deviceId).asJson)
                case None =>
                  BadRequest(ServerError("M_FORBIDDEN", Some(s"No access token for device ${device.deviceId}")).asJson)
              }

          }
      }
    }
  }
}
