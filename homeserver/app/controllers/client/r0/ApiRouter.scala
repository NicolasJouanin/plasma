package controllers.client.r0

import javax.inject.Inject
import play.api.routing.Router.Routes
import play.api.routing.{HandlerDef, SimpleRouter}
import play.api.routing.sird._
import room.RoomController

class ApiRouter @Inject()(authController: AuthController, roomController: RoomController) extends SimpleRouter {
  override def routes: Routes = {
    case GET(p"/login")                        => authController.loginGet
    case POST(p"/login")                       => authController.loginPost
    case POST(p"/logout")                      => authController.logout
    case POST(p"/register" ? q_o"kind=$kind")  => authController.register(kind)
    case POST(p"/register/email/requestToken") => authController.requestToken
    case POST(p"/createRoom")                  => roomController.createRoom
  }
}
