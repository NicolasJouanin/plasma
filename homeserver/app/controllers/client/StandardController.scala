package controllers.client

import io.circe.generic.auto._
import io.circe.syntax._
import io.plasma.matrix.client.r0.model.Versions
import javax.inject.Inject
import play.api.libs.circe.Circe
import play.api.mvc.{BaseController, ControllerComponents}

class StandardController @Inject()(val controllerComponents: ControllerComponents) extends BaseController with Circe {
  def versions = Action {
    Ok(Versions(List("r0.3.0")).asJson)
  }
}
