package controllers

import auth.{Device, DeviceRepo}
import com.typesafe.scalalogging.LazyLogging
import io.circe.Printer
import io.plasma.sdk.matrix.model.ServerError
import javax.inject.Inject
import play.api.mvc._
import play.api.mvc.Results.{BadRequest, _}

import scala.concurrent.{ExecutionContext, Future}
import io.circe.generic.auto._
import io.circe.syntax._

case class AuthenticatedRequest[A](accessToken: String, device: Option[Device], request: Request[A])
    extends WrappedRequest[A](request)

class AuthenticatedAction @Inject()(val parser: BodyParsers.Default, deviceRepo: DeviceRepo)(
    implicit val executionContext: ExecutionContext)
    extends ActionBuilder[AuthenticatedRequest, AnyContent]
    with LazyLogging {
  private val defaultPrinter = Printer.noSpaces.copy(dropNullValues = true)

  def invokeBlock[A](request: Request[A], block: AuthenticatedRequest[A] => Future[Result]): Future[Result] = {
    request.headers.get("Authorization") match {
      case Some(bearer) =>
        val accessToken = bearer.replaceAll("Bearer ", "")
        deviceRepo
          .getByAccessToken(accessToken)
          .flatMap(device => block(AuthenticatedRequest(accessToken, device, request)))
      case None =>
        Future.successful(BadRequest(ServerError(errcode = "M_MISSING_TOKEN").asJson.pretty(defaultPrinter)))
    }
  }
}
