package room

import java.time.LocalDateTime

import akka.actor.Actor
import com.typesafe.scalalogging.LazyLogging
import io.plasma.sdk.matrix.model.ServerError
import javax.inject.Inject
import room.RoomActor._
import room.model.{Event, Room, RoomRepo, RoomVisibility}

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

object RoomActor {
  sealed trait RoomMessage {
    def roomId: String
  }
  case class GetRoomState(roomId: String) extends RoomMessage
  case class CreateRoom(roomId: String,
                        visibility: Option[String],
                        aliasName: Option[String],
                        name: Option[String],
                        topic: Option[String])
      extends RoomMessage
  case class InvalidMessage(currentState: String)

  sealed trait ActorState
  case object Uninitialized extends ActorState
  case object Initialized   extends ActorState
  case class UpdateState(f: RoomState => RoomState)
  case class SetState(s: RoomState)
}

class RoomActor @Inject()(roomRepo: RoomRepo) extends Actor with LazyLogging {
  import context.dispatcher

  private lazy val roomId                  = self.path.name
  private var roomState: Option[RoomState] = None

  override def preStart(): Unit = {
    logger.info(s"Starting Room $roomId actor")

    val roomFuture   = roomRepo.getByRoomId(roomId)
    val eventsFuture = roomRepo.getCurrentRoomState(roomId)
    val f = for {
      room   <- roomFuture
      events <- eventsFuture

    } yield (room, events)
    try {
      Await.result(f, 10.seconds) match {
        case (None, _) =>
          logger.debug(s"Room $roomId doesn't exist yet")
          roomState = None // room doesn't exists
        case (Some(room), events) =>
          logger.debug(s"Found $roomId state: ($room, $events)")
          val stateEvents = events.map(e => (StateKey(e.eventType, e.stateKey), e)).toMap
          roomState = Some(RoomState(room, stateEvents))
        case t @ _ => logger.debug(s"$t")
      }
    } catch {
      case t: Throwable => logger.warn(t.getMessage)
    }
  }

  override def receive: Receive = {
    case c: CreateRoom =>
      val s = sender()
      roomState match {
        case None =>
          createRoom(c)
            .map { room =>
              s ! Right(room)
            }
            .recover {
              case t => s ! Left(ServerError("IO.PLASMA.ROOM.ROOM_ACTOR.CREATE_ROOM", Some(t.getMessage)))
            }
        case Some(state) => s ! state.room
      }
    case msg @ _ if msg.isInstanceOf[RoomMessage] => sender ! Uninitialized
    case UpdateState(f) =>
      roomState = Some(f(roomState.getOrElse(RoomState(room = _initRoom))))
  }

  private def createRoom(createMessage: CreateRoom): Future[Room] = {
    val visibility = createMessage.visibility.getOrElse("private").toLowerCase match {
      case "public"  => RoomVisibility.PUBLIC
      case "private" => RoomVisibility.PRIVATE
    }
    val room = _initRoom.copy(visibility = visibility)
    roomRepo.create(room).map { room =>
      self ! UpdateState(s => s.copy(room = room))
      room
    }
  }

  private def _initRoom = Room(roomId, RoomVisibility.PRIVATE, None, LocalDateTime.now())

}
