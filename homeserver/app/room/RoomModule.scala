package room

import com.google.inject.AbstractModule
import net.codingwell.scalaguice.ScalaModule
import play.api.libs.concurrent.AkkaGuiceSupport

class RoomModule extends AbstractModule with ScalaModule with AkkaGuiceSupport {
  def configure(): Unit = {
    bind[RoomSharding].asEagerSingleton()
    bind[RoomService]
  }
}
