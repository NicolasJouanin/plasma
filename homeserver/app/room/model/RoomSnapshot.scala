package room.model

import java.time.LocalDateTime

case class RoomSnapshot(snapshotId: String, room_id: String, createdAt: LocalDateTime)
