package room.model

import com.typesafe.scalalogging.LazyLogging
import db.DbContext
import javax.inject.Inject

import scala.concurrent.ExecutionContext

class EventRepo @Inject()(val db: DbContext)(implicit ec: ExecutionContext) extends LazyLogging {
  import db._

  val events: db.Quoted[db.EntityQuery[Event]] = quote(querySchema[Event]("events"))
}
