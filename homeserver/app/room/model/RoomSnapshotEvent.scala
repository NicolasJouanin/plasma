package room.model

case class RoomSnapshotEvent(snapshotId: String, eventId: String)
