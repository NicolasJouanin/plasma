package room.model

case class Event(eventId: String, roomId: String, eventType: String, stateKey: Option[String], content: Option[String])
