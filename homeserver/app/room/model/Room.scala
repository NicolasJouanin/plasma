package room.model

import java.time.LocalDateTime

case class Room(roomId: String, visibility: Int, currentSnapshot: Option[String], createdAt: LocalDateTime)

object RoomVisibility {
  val PUBLIC  = 0
  val PRIVATE = 1
}
