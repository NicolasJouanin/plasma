package room.model

import java.time.LocalDateTime

import com.typesafe.scalalogging.LazyLogging
import db.DbContext
import io.plasma.utils.Codecs
import javax.inject.Inject

import scala.concurrent.{ExecutionContext, Future}

class RoomRepo @Inject()(val db: DbContext)(implicit ec: ExecutionContext) extends LazyLogging {
  import db._

  private val rooms               = quote(querySchema[Room]("rooms"))
  private val roomAlias           = quote(querySchema[RoomAlias]("room_alias"))
  private val roomSnapshots       = quote(querySchema[RoomSnapshot]("room_snapshots"))
  private val roomSnapshotsEvents = quote(querySchema[RoomSnapshotEvent]("room_snapshots_events"))
  private val events              = quote(querySchema[Event]("events"))

  def create(room: Room): Future[Room] =
    Future {
      run(rooms.insert(lift(room)))
    }.map(_ => room)

  def getByRoomId(roomId: String): Future[Option[Room]] =
    Future { run(rooms.filter(_.roomId == lift(roomId))) }.map(_.headOption)

  def getRoomWithAlias(roomId: String): Future[List[(Option[Room], RoomAlias)]] =
    Future {
      run(rooms.filter(_.roomId == lift(roomId)).rightJoin(roomAlias).on((room, alias) => room.roomId == alias.roomId))
    }.map(_.toList)

  def getCurrentRoomState(roomId: String): Future[List[Event]] = {
    val q = quote {
      for {
        (_, snapshot) <- rooms
          .filter(_.roomId == lift(roomId))
          .join(roomSnapshots)
          .on((room, s) => room.currentSnapshot.contains(s.snapshotId))
        rse <- roomSnapshotsEvents
          .join(_.snapshotId == snapshot.snapshotId)
        events <- events
          .join(_.eventId == rse.eventId)
      } yield events
    }

    Future { run(q) }.map(_.toList)
  }
}
