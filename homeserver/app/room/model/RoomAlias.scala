package room.model

case class RoomAlias(aliasName: String, roomId: String)
