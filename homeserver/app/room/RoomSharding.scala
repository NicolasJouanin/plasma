package room

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.cluster.sharding.{ClusterSharding, ClusterShardingSettings, ShardRegion}
import com.typesafe.scalalogging.LazyLogging
import javax.inject.Inject
import room.RoomActor.GetRoomState
import room.RoomSharding.RoomEnvelope
import room.model.RoomRepo

object RoomSharding {
  case class RoomEnvelope(roomId: String, payload: Any)
}
class RoomSharding @Inject()(system: ActorSystem, roomRepo: RoomRepo) extends LazyLogging {

  val numberOfShards = 100

  private val extractRoomId: ShardRegion.ExtractEntityId = {
    case RoomEnvelope(id, payload) => (id, payload)
  }

  private val extractShardId: ShardRegion.ExtractShardId = {
    case RoomEnvelope(id, _)         ⇒ (id.hashCode % numberOfShards).toString
    case ShardRegion.StartEntity(id) ⇒
      // StartEntity is used by remembering entities feature
      (id.hashCode % numberOfShards).toString
  }

  val actorRegion: ActorRef = ClusterSharding(system).start(
    typeName = "RoomActor",
    entityProps = Props(classOf[RoomActor], roomRepo),
    settings = ClusterShardingSettings(system),
    extractEntityId = extractRoomId,
    extractShardId = extractShardId
  )
}
