package room

import io.plasma.matrix.client.r0.model.rooms.{CreateRoomRequest, CreateRoomResponse}
import io.plasma.sdk.matrix.model.ServerError
import io.plasma.utils.Codecs
import javax.inject.Inject
import room.RoomActor.{CreateRoom, GetRoomState}
import room.RoomSharding.RoomEnvelope
import room.model.Room
import akka.pattern.ask
import akka.util.Timeout
import com.typesafe.scalalogging.LazyLogging
import server.ServerService

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._

class RoomService @Inject()(roomShard: RoomSharding, serverService: ServerService)(implicit ec: ExecutionContext)
    extends LazyLogging {

  def createRoom(request: CreateRoomRequest): Future[Either[ServerError, CreateRoomResponse]] = {
    val roomId  = s"!${Codecs.genRoomId()}:${serverService.domainName}"
    val newRoom = CreateRoom(roomId, request.visibility, request.room_alias_name, request.name, request.topic)
    logger.debug(s"$newRoom")
    askRoomActor(roomId, newRoom)
      .mapTo[Either[ServerError, Room]]
      .map { res =>
        res.map(room => CreateRoomResponse(room.roomId))
      }
  }

  private def sendRoomActor(roomId: String, payload: Any) = roomShard.actorRegion ! RoomEnvelope(roomId, payload)

  private def askRoomActor(roomId: String, payload: Any) = {
    implicit val timeout = Timeout(30.seconds)
    roomShard.actorRegion ? RoomEnvelope(roomId, payload)
  }
}
