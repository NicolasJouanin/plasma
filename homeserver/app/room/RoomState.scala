package room

import java.time.LocalDateTime

import room.model.{Event, Room, RoomVisibility}

case class StateKey(eventType: String, eventState: Option[String] = None)
case class RoomState(room: Room, stateEvents: Map[StateKey, Event] = Map.empty)
