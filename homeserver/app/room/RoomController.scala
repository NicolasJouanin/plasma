package room

import auth.AuthService
import com.typesafe.scalalogging.LazyLogging
import controllers.AuthenticatedAction
import io.plasma.matrix.client.r0.model.rooms.CreateRoomRequest
import javax.inject.Inject
import play.api.libs.circe.Circe
import play.api.mvc.{Action, BaseController, ControllerComponents}
import io.circe.generic.auto._
import io.circe.syntax._
import io.plasma.sdk.matrix.model.ServerError

import scala.concurrent.ExecutionContext

class RoomController @Inject()(val controllerComponents: ControllerComponents,
                               authenticatedAction: AuthenticatedAction,
                               authService: AuthService,
                               roomService: RoomService)(implicit ec: ExecutionContext)
    extends BaseController
    with Circe
    with LazyLogging {
  def createRoom = Action.async {
    roomService
      .createRoom(CreateRoomRequest("test"))
      .map {
        case Left(e)  => BadRequest(e.asJson)
        case Right(r) => Ok(r.asJson)
      }
      .recover {
        case t => BadRequest(ServerError("IO.PLASMA.HS.INTERNAL_ERROR", Some(t.getMessage)).asJson)
      }
  }
}
