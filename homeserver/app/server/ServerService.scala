package server

import javax.inject.Inject
import settings.SettingsService

import scala.concurrent.{Await, ExecutionContext}
import scala.concurrent.duration._

class ServerService @Inject()(settingsService: SettingsService)(implicit ec: ExecutionContext) {
  val domainName: String = Await.result(settingsService.getAsString("plasma.homeserver.domain.name"), 10.seconds).get
  def getMXID(localPart: String) = {
    s"@$localPart:$domainName"
  }
}
