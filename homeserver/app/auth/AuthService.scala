package auth

import java.time.LocalDateTime

import akka.actor.ActorRef
import auth.AuthActor._
import com.typesafe.scalalogging.LazyLogging
import io.plasma.matrix.client.r0.model.auth.AuthFlow
import io.plasma.sdk.matrix.model.ServerError
import io.plasma.utils.{Codecs, MatrixUtils}
import javax.inject.{Inject, Named}
import server.ServerService
import settings.SettingsService

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

case class CheckAuthFlowResult(authResultFlow: Option[AuthFlow] = None, authError: Option[ServerError] = None)

class AuthService @Inject()(@Named("authActor") authActor: ActorRef,
                            settingsService: SettingsService,
                            accountRepo: AccountRepo,
                            serverService: ServerService,
                            deviceRepo: DeviceRepo)(implicit ec: ExecutionContext)
    extends LazyLogging {
  import akka.pattern.ask
  import io.plasma.utils.actors.defaults._

  def genAuthFlow(endpoint: String): Future[AuthFlow] = {
    endpoint match {
      case "/register" | "/login" => (authActor ? GenRegistrationFlow(endpoint.substring(1))).mapTo[AuthFlow]
      case _                      => Future.failed(new IllegalArgumentException(s"Invalid endpoint $endpoint"))
    }
  }

  def checkAuth(authData: Map[String, String]): Future[CheckAuthFlowResult] = {
    for {
      authSession <- getAuthSession(authData)
      authResult  <- doAuthStage(authData, authSession)
    } yield authResult
  }

  def createAccount(username: Option[String],
                    password: Option[String],
                    isGuest: Boolean = false,
                    isAdmin: Boolean = false,
                    displayName: Option[String] = None,
                    avatarUtl: Option[String] = None): Future[Either[ServerError, Account]] = {
    val passwordFuture = password match {
      case Some(p) => hashPassword(p).map(Some(_))
      case None    => Future.successful(None)
    }
    val userIdFuture = username
      .map(validateUserId)
      .getOrElse(Future.successful(Right(s"@${Codecs.genUserId()}:${serverService.domainName}")))

    val f = for {
      password <- passwordFuture
      userId   <- userIdFuture
    } yield (password, userId)

    f.flatMap {
      case (_, Left(error)) => Future.successful(Left(error))
      case (p, Right(userId)) =>
        accountRepo
          .create(Account(userId, p, isAdmin, isGuest, LocalDateTime.now(), displayName, avatarUtl, None))
          .map(Right(_))
    }
  }

  private def validateUserId(userId: String): Future[Either[ServerError, String]] = {
    val userIdPattern = "[\\p{Lower}\\d\\._=-]*".r
    MatrixUtils.isValidMxId(userId) match {
      case true =>
        accountRepo.getByMxId(userId).map {
          case Some(_) => Left(ServerError("M_USER_IN_USE"))
          case None    => Right(userId)
        }
      case false =>
        logger.debug(s"Invalid username $userId")
        Future.successful(Left(ServerError("M_INVALID_USERNAME")))
    }
  }

  def createDevice(deviceId: Option[String], displayName: Option[String], owner: Account): Future[Device] = {
    val id = deviceId.getOrElse(Codecs.genDeviceId())
    deviceRepo.create(Device(id, displayName, owner.mxId, LocalDateTime.now(), None, Some(Codecs.genAccessToken())))
  }

  def getOrCreateDevice(owner: Account, deviceId: Option[String], displayName: Option[String]): Future[Device] = {
    deviceId match {
      case None => createDevice(None, displayName, owner)
      case Some(id) =>
        deviceRepo.getByDeviceId(id).flatMap {
          case None         => createDevice(None, displayName, owner)
          case Some(device) => Future.successful(device)
        }
    }
  }

  def logout(accessToken: String): Future[Unit] = deviceRepo.updateAccessToken(accessToken).map(_ => Unit)

  private def hashPassword(password: String): Future[String] = (authActor ? HashPassword(password)).mapTo[String]

  private def doAuthStage(authData: Map[String, String], authSessionFlow: AuthFlow): Future[CheckAuthFlowResult] = {
    validateAuthType(authData) match {
      case Failure(t) =>
        Future.successful(
          CheckAuthFlowResult(authError = Some(ServerError("IO.PLASMA.HS.AUTH.ILLEGAL_ARGUMENT", Some(t.getMessage)))))
      case Success(authType) =>
        if (authSessionFlow.completed.isDefined && authSessionFlow.completed.get.contains(authType)) {
          Future.successful(
            CheckAuthFlowResult(
              authResultFlow = Some(authSessionFlow.copy(errcode = Some("IO.PLASMA.HS.AUTH.ALREADY_COMPLETED"),
                                                         error = Some(s"'$authType' is already completed")))))
        } else {
          val stages = nextExpectedStages(authSessionFlow)
          if (!stages.filter(_.isDefined).map(_.get).contains(authType)) {
            Future.successful(
              CheckAuthFlowResult(
                authError = Some(
                  ServerError("IO.PLASMA.HS.AUTH.ILLEGAL_ARGUMENT",
                              Some(s"Unexpected auth type '$authType'. Expecting one of ${stages.mkString(",")}")))))
          } else {
            val authResult = authType match {
              case "m.login.password" => doMLoginPasswordAuth(authData, authSessionFlow)
              case "m.login.dummy"    => doMLoginDummyAuth(authData, authSessionFlow)
              case "m.login.recaptcha" =>
                Future.successful(Some(authSessionFlow.copy(errcode = Some("IO.PLASMA.HS.NOT_IMPLEMENTED"))))
              case "m.login.oauth2" =>
                Future.successful(Some(authSessionFlow.copy(errcode = Some("IO.PLASMA.HS.NOT_IMPLEMENTED"))))
              case "m.login.email.identity" =>
                Future.successful(Some(authSessionFlow.copy(errcode = Some("IO.PLASMA.HS.NOT_IMPLEMENTED"))))
              case "m.login.token" =>
                Future.successful(Some(authSessionFlow.copy(errcode = Some("IO.PLASMA.HS.NOT_IMPLEMENTED"))))
            }
            authResult.map {
              case Some(flow) =>
                CheckAuthFlowResult(authResultFlow = Some(flow))
              case None =>
                //Auth stage completed successfully (no auth auth)
                val nextFlow =
                  authSessionFlow.copy(
                    completed = Some(authSessionFlow.completed.getOrElse(List.empty) ++ List(authType)))
                authActor ! StoreAuthFlowSession(nextFlow)
                logger.debug(s"authFlow after $authType auth: $nextFlow")
                logger.debug(s"remaining stages steps: ${nextExpectedStages(nextFlow)}")
                nextExpectedStages(nextFlow).filter(_.isEmpty) match {
                  case List() =>
                    CheckAuthFlowResult(authResultFlow = Some(nextFlow)) //No empty flow among remaining => more auth steps are neededstages
                  case _ =>
                    authActor ! SessionTimeout(nextFlow.session.get) //Force session, timeout
                    CheckAuthFlowResult() //at least one flow has no more steps to complte, to auth is completed
                }
            }
          }
        }
    }
  }

  private def validateAuthType(authData: Map[String, String]): Try[String] = {
    authData.get("type") match {
      case None                                          => Failure(new IllegalArgumentException("Auth type missing"))
      case Some(session) if session.isEmpty              => Failure(throw new IllegalArgumentException("Auth type empty"))
      case Some(t) if AuthService.AUTH_TYPES.contains(t) => Success(t)
      case Some(t)                                       => Failure(throw new IllegalArgumentException(s"Unexpected auth type '$t'"))
    }
  }

  private def nextExpectedStages(authSessionFlow: AuthFlow) = {
    val completedFlow = authSessionFlow.completed.getOrElse(List.empty)
    authSessionFlow.flows
      .map { flow =>
        flow.stages.diff(completedFlow).headOption
      }
  }

  private def doMLoginPasswordAuth(authData: Map[String, String],
                                   authSessionFlow: AuthFlow): Future[Option[AuthFlow]] = {
    val user     = authData.get("user")
    val password = authData.get("password")
    (user, password) match {
      case (Some(u), Some(p)) =>
        checkLoginPassword(u, p) map {
          case Left(loginError) =>
            Some(authSessionFlow.copy(errcode = Some(loginError.errcode), error = loginError.error))
          case Right(_) => None
        }
      case (None, _) =>
        Future.successful(Some(authSessionFlow.copy(errcode = Some("M_FORBIDDEN"), error = Some("Invalid user"))))
      case (_, None) =>
        Future.successful(Some(authSessionFlow.copy(errcode = Some("M_FORBIDDEN"), error = Some("Invalid password"))))
    }
  }

  def checkLoginPassword(localPart: String, password: String): Future[Either[ServerError, Account]] = {
    accountRepo.getByMxId(serverService.getMXID(localPart)).flatMap {
      case None =>
        Future.successful(Left(ServerError(errcode = "M_FORBIDDEN", error = Some("Unknown user"))))
      case Some(account) =>
        account.passwordHash match {
          case None =>
            Future.successful(Left(ServerError(errcode = "M_FORBIDDEN", error = Some("No password set"))))
          case Some(hash) =>
            (authActor ? VerifyPassword(hash, password)).mapTo[Boolean].map {
              case false =>
                Left(ServerError(errcode = "M_FORBIDDEN", error = Some("Wrong password")))
              case true => Right(account)
            }
        }
    }
  }

  private def doMLoginDummyAuth(authData: Map[String, String], authSessionFlow: AuthFlow): Future[Option[AuthFlow]] =
    Future.successful(None)

  private def getAuthSession(authData: Map[String, String]): Future[AuthFlow] = {
    authData.get("session") match {
      case None                             => Future.failed(new IllegalArgumentException(s"Auth session missing"))
      case Some(session) if session.isEmpty => Future.failed(new IllegalArgumentException(s"Auth session empty"))
      case Some(session) =>
        (authActor ? GetAuthFlowSession(session)).mapTo[Option[AuthFlow]].map {
          case None       => throw new IllegalArgumentException(s"Session unknown or expired")
          case Some(flow) => flow
        }
    }
  }

}

object AuthService {
  val AUTH_TYPES = List("m.login.password",
                        "m.login.recaptcha",
                        "m.login.oauth2",
                        "m.login.email.identity",
                        "m.login.token",
                        "m.login.dummy")

}
