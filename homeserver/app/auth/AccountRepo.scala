package auth

import com.typesafe.scalalogging.LazyLogging
import db.DbContext
import javax.inject.Inject

import scala.concurrent.{ExecutionContext, Future}

class AccountRepo @Inject()(val db: DbContext)(implicit ec: ExecutionContext) extends LazyLogging {
  import db._

  private val accounts = quote(querySchema[Account]("accounts"))

  def create(account: Account): Future[Account] =
    Future {
      run(accounts.insert(lift(account)))
    }.map(_ => account)

  def getByMxId(mxid: String): Future[Option[Account]] =
    Future { run(accounts.filter(_.mxId == lift(mxid))) }.map(r => r.headOption)

}
