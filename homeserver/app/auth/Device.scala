package auth

import java.time.LocalDateTime

case class Device(deviceId: String,
                  displayName: Option[String],
                  ownerAccountMxId: String,
                  createdAt: LocalDateTime,
                  updatedAt: Option[LocalDateTime],
                  accessToken: Option[String])
