package auth

import java.time.LocalDateTime

import com.typesafe.scalalogging.LazyLogging
import db.DbContext
import javax.inject.Inject

import scala.concurrent.{ExecutionContext, Future}

class DeviceRepo @Inject()(val db: DbContext)(implicit ec: ExecutionContext) extends LazyLogging {
  import db._

  private val devices = quote(querySchema[Device]("devices"))

  def create(device: Device): Future[Device] =
    Future {
      run(devices.insert(lift(device)))
    }.map(_ => device)

  def getByDeviceId(deviceId: String): Future[Option[Device]] =
    Future { run(devices.filter(_.deviceId == lift(deviceId))) }.map(r => r.headOption)

  def getByAccessToken(accessToken: String): Future[Option[Device]] =
    Future { run(devices.filter(_.accessToken.exists(_ == lift(accessToken)))) }.map(r => r.headOption)

  def updateAccessToken(accessTokenFilter: String, newToken: Option[String] = None) = Future {
    run(
      devices
        .filter(_.accessToken.exists(_ == lift(accessTokenFilter)))
        .update(_.accessToken -> lift(newToken), _.updatedAt -> lift(Option(LocalDateTime.now()))))
  }
}
