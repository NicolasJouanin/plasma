package auth

import java.time.LocalDateTime

case class Account(mxId: String,
                   passwordHash: Option[String],
                   isAdmin: Boolean,
                   isGuest: Boolean,
                   createdAt: LocalDateTime,
                   displayName: Option[String],
                   avatarUrl: Option[String],
                   updatedAt: Option[LocalDateTime])
