package auth

case class ContactInfo(info_id: String, accountMxId: String, medium: String, address: String, isThreePid: Boolean)
