package auth

import akka.actor.{Actor, ActorLogging, Timers}
import de.mkammerer.argon2.{Argon2Factory, Argon2Helper}
import io.plasma.matrix.client.r0.model.auth.{AuthFlow, Flow}
import io.plasma.utils.{Codecs, MapUtils}
import io.plasma.utils.actors.SetCacheDuration
import javax.inject.Inject
import settings.SettingsService
import auth.AuthActor._

import scala.concurrent.duration.{Duration, FiniteDuration}
import scala.concurrent.duration._

object AuthActor {
  case class GenRegistrationFlow(endpoint: String)
  case class StoreAuthFlowSession(flow: AuthFlow)
  case class GetAuthFlowSession(session: String)
  case class SessionTimeout(session: String)
  case class SetCacheDuration(duration: FiniteDuration)
  case class SetHashParams(timeCost: Long, memCost: Int, parallelism: Int, saltLength: Int, hashLength: Int)
  case class HashPassword(password: String)
  case class VerifyPassword(hash: String, password: String)

  val DEFAULT_CACHE_DURATION: FiniteDuration = 10.minutes
  val DEFAULT_HASH_TIME_COST                 = 1000L //1000ms
  val DEFAULT_HASH_MEM_COST                  = 65535
  val DEFAULT_HASH_PARALLELISM               = 1
  val DEFAULT_HASH_SALT_LENGTH               = 128
  val DEFAULT_HASH_HASH_LENGTH               = 128
  val DEFAULT_HASH_ITERATIONS                = 2
}

class AuthActor @Inject()(settingsService: SettingsService) extends Actor with Timers with ActorLogging {

  import context.dispatcher
  import akka.pattern.pipe

  private var cacheDuration: FiniteDuration                                = Duration.Zero
  private val sessionCache: scala.collection.mutable.Map[String, AuthFlow] = scala.collection.mutable.Map.empty
  private var argon2                                                       = Argon2Factory.create()
  private var argon2Iterations                                             = AuthActor.DEFAULT_HASH_ITERATIONS
  private var argon2Memory                                                 = AuthActor.DEFAULT_HASH_MEM_COST
  private var argon2Parallelism                                            = AuthActor.DEFAULT_HASH_PARALLELISM

  override def preStart(): Unit = {
    settingsService.getAsMinutes("plasma.homeserver.auth.session.cache.duration").map {
      case Some(duration) => self ! SetCacheDuration(duration)
      case None           => self ! SetCacheDuration(AuthActor.DEFAULT_CACHE_DURATION)
    }
    val hashParamsFutures = for {
      timeCost    <- settingsService.getAsLong("plasma.homeserver.auth.hash.time_cost")
      memCost     <- settingsService.getAsInt("plasma.homeserver.auth.hash.mem_cost")
      parallelism <- settingsService.getAsInt("plasma.homeserver.auth.hash.parallelism")
      saltLength  <- settingsService.getAsInt("plasma.homeserver.auth.hash.salt_length")
      hashLength  <- settingsService.getAsInt("plasma.homeserver.auth.hash.hash_length")
    } yield (timeCost, memCost, parallelism, saltLength, hashLength)
    hashParamsFutures.map {
      case (t, m, p, s, h) =>
        self ! SetHashParams(
          t.getOrElse(AuthActor.DEFAULT_HASH_TIME_COST),
          m.getOrElse(AuthActor.DEFAULT_HASH_MEM_COST),
          p.getOrElse(AuthActor.DEFAULT_HASH_PARALLELISM),
          s.getOrElse(AuthActor.DEFAULT_HASH_SALT_LENGTH),
          h.getOrElse(AuthActor.DEFAULT_HASH_HASH_LENGTH)
        )
    }
  }

  override def receive = {
    case GenRegistrationFlow(endpoint) =>
      getEndPointFlows(endpoint)
        .map { flow =>
          self ! StoreAuthFlowSession(flow)
          flow
        }
        .pipeTo(sender())
    case StoreAuthFlowSession(flow) =>
      val session = flow.session.get
      sessionCache += (session -> flow)
      log.debug(s"session $session added to cache. Current cache size: ${sessionCache.size}.")

      timers.startSingleTimer(session, SessionTimeout(session), cacheDuration)
    case SessionTimeout(session) =>
      sessionCache -= session
      log.debug(s"session $session times out, delete from cache. Current cache size: ${sessionCache.size}")
    case SetCacheDuration(duration) =>
      cacheDuration = duration
      log.debug(s"session cache duration set to ${cacheDuration}")
    case GetAuthFlowSession(session) => sender ! sessionCache.get(session)
    case SetHashParams(t, m, p, s, h) =>
      argon2 = Argon2Factory.create(s, h)
      argon2Memory = m
      argon2Parallelism = p
      argon2Iterations = Argon2Helper.findIterations(argon2, t, argon2Memory, argon2Parallelism)
    case HashPassword(password)         => sender ! argon2.hash(argon2Iterations, argon2Memory, argon2Parallelism, password)
    case VerifyPassword(hash, password) => sender ! argon2.verify(hash, password)
  }

  private def getEndPointFlows(endpoint: String) = {
    val flowFuture = settingsService.filterSettings(s"plasma.homeserver.auth.$endpoint.flows.%").map { flowSettings =>
      if (flowSettings.isEmpty)
        log.warning(
          s"No auth flow configured for '$endpoint' endpoint. Consider adding some 'plasma.homeserver.auth.$endpoint.flows' settings")
      flowSettings.map { s =>
        Flow(s.getStringValue().replaceAll(" ", "").split(",").toList)
      }
    }
    val paramFuture = getEndPointAuthTypesParams(endpoint)
    for {
      flows  <- flowFuture
      params <- paramFuture
    } yield AuthFlow(flows = flows, params = params, session = Some(Codecs.genSessionId()))
  }

  /**
    * Setting pattern : plasma.homeserver.auth.$endpoint.params.m.login.dummy.some_param=someValue
    * @param endpoint
    * @return
    */
  private def getEndPointAuthTypesParams(endpoint: String) = {
    val settingPath = s"plasma.homeserver.auth.$endpoint.params."
    settingsService.filterSettings(s"$settingPath%").map { settings =>
      val triplet = settings
        .map { s =>
          val paramElems = s.path.drop(settingPath.length).split("\\.")
          val authType   = paramElems.dropRight(1).mkString(".")
          val paramName  = paramElems.reverse.take(1).head
          (authType, paramName, s.getStringValue())
        }
      MapUtils.tripletToMap(triplet)
    }
  }
}
