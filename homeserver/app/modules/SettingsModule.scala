package modules

import com.google.inject.AbstractModule
import net.codingwell.scalaguice.ScalaModule
import play.api.{Configuration, Environment}
import play.api.libs.concurrent.AkkaGuiceSupport
import settings.{SettingsActor, SettingsService}

class SettingsModule(environment: Environment, config: Configuration)
    extends AbstractModule
    with ScalaModule
    with AkkaGuiceSupport {
  def configure() = {
    bind[SettingsService]
    bindActor[SettingsActor]("settingsActor")
  }
}
