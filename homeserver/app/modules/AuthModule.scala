package modules

import auth.{AuthActor, AuthService}
import com.google.inject.AbstractModule
import net.codingwell.scalaguice.ScalaModule
import play.api.libs.concurrent.AkkaGuiceSupport

class AuthModule extends AbstractModule with ScalaModule with AkkaGuiceSupport {
  def configure(): Unit = {
    bind[AuthService]
    bindActor[AuthActor]("authActor")
  }
}
