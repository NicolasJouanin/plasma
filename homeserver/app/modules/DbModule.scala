package modules

import com.google.inject.{AbstractModule, Provides}
import com.zaxxer.hikari.HikariDataSource
import db.DbContext
import io.getquill.{PostgresJdbcContext, SnakeCase}
import net.codingwell.scalaguice.ScalaModule
import play.api.db.Database
import play.api.libs.concurrent.AkkaGuiceSupport
import play.api.{Configuration, Environment}

class DbModule(environment: Environment, config: Configuration)
    extends AbstractModule
    with ScalaModule
    with AkkaGuiceSupport {
  def configure() = {}

  @Provides
  def provideDBContext(db: Database): DbContext =
    new PostgresJdbcContext(SnakeCase, db.dataSource.asInstanceOf[HikariDataSource])

}
