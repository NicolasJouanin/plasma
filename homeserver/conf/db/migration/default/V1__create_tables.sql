CREATE TABLE settings (
    path VARCHAR PRIMARY KEY,
    string_value VARCHAR,
    default_string_value VARCHAR
);

INSERT INTO settings VALUES('plasma.homeserver.domain.name', null, 'localhost');
INSERT INTO settings VALUES('plasma.homeserver.auth.login.flows.0', null, 'm.login.password');
INSERT INTO settings VALUES('plasma.homeserver.auth.register.flows.0', null, 'm.login.dummy');
INSERT INTO settings VALUES('plasma.homeserver.auth.register.flows.1', null, 'm.login.email.identity,m.login.recaptcha');
INSERT INTO settings VALUES('plasma.homeserver.auth.register.params.m.login.recaptcha.public_key', null, 'some_public_key');
INSERT INTO settings VALUES('plasma.homeserver.auth.session.cache.duration', null, '10');

CREATE TABLE accounts (
  mx_id VARCHAR PRIMARY KEY,
  password_hash VARCHAR,
  is_admin BOOLEAN,
  is_guest BOOLEAN,
  display_name VARCHAR,
  avatar_url VARCHAR,
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP
);

CREATE TABLE devices (
  device_id VARCHAR PRIMARY KEY,
  display_name VARCHAR,
  owner_account_mx_id VARCHAR REFERENCES accounts(mx_id),
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP,
  access_token VARCHAR
);

CREATE TABLE contact_info (
  info_id VARCHAR PRIMARY KEY,
  account_mx_id VARCHAR REFERENCES accounts(mx_id),
  medium VARCHAR NOT NULL,
  address VARCHAR,
  is_three_pid BOOLEAN
);

CREATE TABLE rooms (
  room_id VARCHAR PRIMARY KEY,
  visibility INT NOT NULL,
  current_snapshot VARCHAR,
  created_at TIMESTAMP NOT NULL
);

CREATE TABLE room_alias (
  alias VARCHAR PRIMARY KEY,
  room_id VARCHAR REFERENCES rooms(room_id)
);

CREATE TABLE room_snapshots(
  snapshot_id VARCHAR PRIMARY KEY,
  room_id VARCHAR REFERENCES rooms(room_id),
  created_at TIMESTAMP NOT NULL
);

CREATE TABLE room_snapshots_events(
  snapshot_id VARCHAR REFERENCES room_snapshots(snapshot_id),
  event_id VARCHAR
);


CREATE TABLE events (
  event_id VARCHAR PRIMARY KEY,
  room_id VARCHAR REFERENCES rooms(room_id),
  event_type VARCHAR NOT NULL,
  state_key VARCHAR,
  content JSONB
);

ALTER TABLE rooms ADD CONSTRAINT room_snapshot_snapshot_id_fk FOREIGN KEY (current_snapshot) REFERENCES room_snapshots(snapshot_id);
ALTER TABLE room_snapshots_events ADD CONSTRAINT events_event_id_fk FOREIGN KEY (event_id) REFERENCES events(event_id);