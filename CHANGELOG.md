# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
#### client-server specification
- [Base API](https://matrix.org/docs/spec/client_server/r0.3.0.html#api-standards) bindings.
- [Login API](https://matrix.org/docs/spec/client_server/r0.3.0.html#login) bindings.
- [Account API](https://matrix.org/docs/spec/client_server/r0.3.0.html#account-registration-and-management) bindings.
- [Filtering API](https://matrix.org/docs/spec/client_server/r0.3.0.html#filtering) bindings.
- [Events API](https://matrix.org/docs/spec/client_server/r0.3.0.html#events) bindings.
- [Rooms API](https://matrix.org/docs/spec/client_server/r0.3.0.html#rooms) bindings.
- [Profiles API](https://matrix.org/docs/spec/client_server/r0.3.0.html#profiles) bindings.
- [User-interactive authentication API](https://matrix.org/docs/spec/client_server/r0.3.0.html#user-interactive-authentication-api) server implementation.
- `GET /_matrix/client/versions` server endpoint.
- `GET /_matrix/client/r0/login` server endpoint.
- `POST /_matrix/client/r0/login` server endpoint.
- `POST /_matrix/client/r0/logout` server endpoint.

### To be done
- Modules bindingd
