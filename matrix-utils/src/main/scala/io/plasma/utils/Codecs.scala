package io.plasma.utils

import java.nio.ByteBuffer
import java.util.{Base64, UUID}

import org.apache.commons.codec.binary.Base32

object Codecs {
  private def toBase64(uuid: UUID, unpadded: Boolean): String = {
    val (high, low) =
      (uuid.getMostSignificantBits, uuid.getLeastSignificantBits)
    val buffer = ByteBuffer.allocate(java.lang.Long.BYTES * 2)
    buffer.putLong(high)
    buffer.putLong(low)
    toBase64(buffer.array(), unpadded)
  }

  private def toBase32(uuid: UUID, unpadded: Boolean): String = {
    val (high, low) =
      (uuid.getMostSignificantBits, uuid.getLeastSignificantBits)
    val buffer = ByteBuffer.allocate(java.lang.Long.BYTES * 2)
    buffer.putLong(high)
    buffer.putLong(low)
    toBase32(buffer.array(), unpadded)
  }

  private def toBase64(bytes: Array[Byte], unpadded: Boolean): String = unpadded match {
    case false => Base64.getUrlEncoder.encodeToString(bytes)
    case true  => Base64.getUrlEncoder.encodeToString(bytes).split("=")(0)
  }

  private def toBase32(bytes: Array[Byte], unpadded: Boolean): String = {
    val base = new Base32()
    unpadded match {
      case false => base.encodeAsString(bytes)
      case true  => base.encodeAsString(bytes).split("=")(0)
    }

  }

  private def fromBase64(str: String): Array[Byte] = Base64.getUrlDecoder.decode(str)

  private def toHex(bytes: Array[Byte], sep: String = ""): String =
    bytes.map("%02x".format(_)).mkString(sep)

  private def fromHex(hex: String): Array[Byte] = {
    hex
      .replaceAll("[^0-9A-Fa-f]", "")
      .sliding(2, 2)
      .toArray
      .map(Integer.parseInt(_, 16).toByte)
  }

  def randomUUID: String = toBase64(UUID.randomUUID(), false)

  def genSessionId(): String = toBase64(UUID.randomUUID(), true)

  def genDeviceId(): String = toBase64(UUID.randomUUID(), true)

  def genAccessToken(): String = toBase64(UUID.randomUUID(), true)

  def genUserId(): String = toBase32(UUID.randomUUID(), true).toLowerCase()

  def genRoomId(): String = toBase64(UUID.randomUUID(), true)

  def genId(): String = toBase64(UUID.randomUUID(), true)
}
