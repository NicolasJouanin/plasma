package io.plasma.utils

object MatrixUtils {
  private val UserIdPattern = """@([\p{Lower}\d\._=-]*):(.+)""".r
  def userIdLocalPart(userId: String): Option[String] = {
    UserIdPattern.findFirstMatchIn(userId) match {
      case Some(m) => Some(m.group(1))
      case _       => None
    }
  }

  def isValidMxId(mxid: String): Boolean = {
    mxid match {
      case UserIdPattern(_*) => true
      case _                 => false
    }
  }
}
