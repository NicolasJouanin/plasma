package io.plasma.utils

object MapUtils {
  def tripletToMap[T](triplet: List[(T, T, T)]): Map[T, Map[T, T]] =
    triplet
      .groupBy(t => t._1)
      .mapValues(v => v.map(vv => (vv._2, vv._3)).groupBy(t => t._1).mapValues(vvv => vvv.map(_._2).head))
}
