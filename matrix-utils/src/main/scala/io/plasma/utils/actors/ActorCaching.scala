package io.plasma.utils.actors

import akka.actor.{Actor, ActorLogging, Timers}

import scala.collection.mutable.Map
import scala.concurrent.Future
import scala.concurrent.duration.{Duration, FiniteDuration}

case class SetCacheDuration(duration: FiniteDuration)

trait ActorCaching[K, V] extends ActorLogging with Timers { this: Actor =>
  import akka.pattern.pipe
  import context.dispatcher

  var cacheDuration: FiniteDuration = Duration.Zero

  private case class NoCache[V](value: V)
  private case class Cache(key: K, value: V)
  private case class TimeOut(key: K)
  private val cache: Map[K, V] = Map.empty

  override def preStart(): Unit = {
    if (!cacheEnabled)
      log.info("Cache is currently disabled")
    else
      log.info(s"Cache is set to $cacheDuration")
  }

  override def receive: Receive = {
    case SetCacheDuration(duration) =>
      cacheDuration = duration
      log.info(s"Cache is set to $cacheDuration")
    case key: K if cache.contains(key) ⇒
      timers.startSingleTimer(key, TimeOut(key), cacheDuration)
      sender ! cache(key)
    case NoCache(v)                ⇒ sender ! v
    case Cache(key, valueResponse) ⇒ onMessageWithResponse(key, valueResponse)
    case TimeOut(key)              => cache -= key
  }

  protected final def respond(futureValue: Future[V]): Unit = futureValue.map(NoCache(_)).pipeTo(self)(sender())
  protected final def cacheAndRespond(key: K, valueFuture: Future[V]): Unit = {
    valueFuture
      .map(Cache(key, _))
      .pipeTo(self)(sender())
    valueFuture
      .recoverWith {
        case t => Future.failed(t)
      }
      .pipeTo(sender())
  }

  private def cacheEnabled: Boolean = cacheDuration != Duration.Zero
  private def onMessageWithResponse(key: K, value: V): Unit = {
    if (cacheEnabled) {
      cache += (key -> value)
      timers.startSingleTimer(key, TimeOut(key), cacheDuration)
    }
    log.debug(s"$sender")
    sender ! value
  }
}
