package io.plasma.utils

import akka.util.Timeout
import scala.concurrent.duration._

package object actors {
  object defaults {
    implicit val DEFAULT_ASK_TIMEOUT = Timeout(10 seconds)
  }

}
