package io.plasma.utils

import org.scalatest.{Matchers, WordSpecLike}

class TestMatrixUtils extends WordSpecLike with Matchers {
  "userIdLocalPart" must {
    "return localPart with full userId" in {
      val localPart = MatrixUtils.userIdLocalPart("@test:matrix.org")
      localPart shouldBe Some("test")
    }

    "return localPart with full userId2" in {
      val localPart = MatrixUtils.userIdLocalPart("@test123sdf45684_:matrix.org")
      localPart shouldBe Some("test123sdf45684_")
    }

    "return none when not matching user patter" in {
      val localPart = MatrixUtils.userIdLocalPart("@something!else")
      localPart shouldBe None
    }
  }

  "isValidMxId" must {
    "return true with @test:matrix.org" in {
      MatrixUtils.isValidMxId("@test:matrix.org") shouldBe true
    }

    "return false with test:matrix.org" in {
      MatrixUtils.isValidMxId("test:matrix.org") shouldBe false
    }

    "return true with @test" in {
      MatrixUtils.isValidMxId("@test") shouldBe false
    }
  }
}
