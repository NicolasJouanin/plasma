package io.plasma.utils

import org.scalatest.{Matchers, WordSpecLike}

class TestMapUtils extends WordSpecLike with Matchers {
  "MapUtils tripletToMap" must {
    "return empty Map" in {
      MapUtils.tripletToMap(List(("", "", ""))) shouldBe Map("" -> Map("" -> ""))
    }

    "return correct Map" in {
      MapUtils.tripletToMap(List(("m.login.dummy", "param1", "value1"), ("m.login.dummy", "param2", "value2"))) shouldBe Map(
        "m.login.dummy" -> Map("param1" -> "value1", "param2" -> "value2"))
    }
    "return correct Map too" in {
      val ret = MapUtils.tripletToMap(
        List(("m.login.recaptcha", "param1", "value1"),
             ("m.login.dummy", "param1", "value1"),
             ("m.login.dummy", "param2", "value2"))) shouldBe Map("m.login.dummy" -> Map("param1" -> "value1",
                                                                                         "param2" -> "value2"),
                                                                  "m.login.recaptcha" -> Map("param1" -> "value1"))
    }
  }
}
