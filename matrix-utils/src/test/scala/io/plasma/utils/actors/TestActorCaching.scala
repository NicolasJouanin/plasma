package io.plasma.utils.actors

import akka.actor.{Actor, ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

import scala.concurrent.Future
import scala.concurrent.duration._

class TestActor extends Actor with ActorCaching[String, String] {
  import context.dispatcher
  override def receive: Receive = super.receive orElse {
    case (key: String, value: String) => cacheAndRespond(key, Future.successful(value))
  }
}

class TestActorCaching
    extends TestKit(ActorSystem("MySpec"))
    with ImplicitSender
    with WordSpecLike
    with Matchers
    with BeforeAndAfterAll {

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  "A cache actor" must {
    "cache key/value" in {
      val cache = system.actorOf(Props[TestActor])
      cache ! ("TestKey", "TestValue")
      expectMsg("TestValue")
    }
  }
}
