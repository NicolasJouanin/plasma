package io.plasma.matrix.identity.client.unstable.api

import akka.actor.ActorSystem
import akka.http.scaladsl.model.Uri
import akka.stream.Materializer
import io.plasma.sdk.matrix.Transport

import scala.concurrent.{ExecutionContextExecutor, Future}

trait BaseApi { self: Transport =>
  implicit def system: ActorSystem
  implicit def materializer: Materializer
  implicit def executionContext: ExecutionContextExecutor

  protected def apiRoot: Uri

  def status(): Future[Unit] = doGet[Unit](apiRoot.withPath(apiRoot.path + "/api/v1"))

}
