package io.plasma.matrix.identity.client.unstable.api

import akka.actor.ActorSystem
import akka.http.scaladsl.model.Uri
import akka.stream.Materializer
import io.plasma.matrix.identity.model.association.LookupResponse
import io.plasma.sdk.matrix.Transport

import scala.concurrent.{ExecutionContextExecutor, Future}

trait AssociationApi { self: Transport =>
  implicit def system: ActorSystem
  implicit def materializer: Materializer
  implicit def executionContext: ExecutionContextExecutor

  protected def apiRoot: Uri

  def lookup(medium: String, address: String): Future[LookupResponse] =
    doGet[LookupResponse](apiRoot.withPath(apiRoot.path + "/api/v1/lookup"),
                          Map("medium" -> medium, "address" -> address))

}
