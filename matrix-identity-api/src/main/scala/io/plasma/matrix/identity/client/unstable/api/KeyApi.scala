package io.plasma.matrix.identity.client.unstable.api

import akka.actor.ActorSystem
import akka.http.scaladsl.model.Uri
import akka.stream.Materializer
import io.plasma.matrix.identity.model.key.{IsValidResponse, PubKeyResponse}
import io.plasma.sdk.matrix.Transport

import scala.concurrent.{ExecutionContextExecutor, Future}

trait KeyApi { self: Transport =>
  implicit def system: ActorSystem
  implicit def materializer: Materializer
  implicit def executionContext: ExecutionContextExecutor

  protected def apiRoot: Uri

  def getPublicKey(keyId: String): Future[PubKeyResponse] =
    doGet[PubKeyResponse](apiRoot.withPath(apiRoot.path + s"/api/v1/pubkey/$keyId"))

  def isPublicKeyValid(keyId: String): Future[IsValidResponse] =
    doGet[IsValidResponse](apiRoot.withPath(apiRoot.path + "/api/v1/pubkey/isvalid"), Map("public_key" -> keyId))

  def isEphemeralKeyValid(keyId: String): Future[IsValidResponse] =
    doGet[IsValidResponse](apiRoot.withPath(apiRoot.path + "/api/v1/pubkey/ephemeral/isvalid"),
                           Map("public_key" -> keyId))
}
